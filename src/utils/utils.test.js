// eslint-disable-next-line no-unused-vars
/* global describe, it, expect */
import { getRandomId } from './index'

describe('Utils', () => {
  it('getRandomId', () => {
    expect(getRandomId()).not.toBeNull()
  })
})
