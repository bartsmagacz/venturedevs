import React from 'react'
import PropTypes from 'prop-types'
import { Grid, TextField } from '@material-ui/core'

export const SearchBar = ({ searchItem, setSearchItem }) => (
  <Grid item sm={2}>
    <TextField label={'Search'} value={searchItem} onChange={(event) => setSearchItem(event.target.value)} />
  </Grid>
)

SearchBar.propTypes = {
  searchItem: PropTypes.string,
  setSearchItem: PropTypes.func
}
