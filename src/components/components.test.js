// eslint-disable-next-line no-unused-vars
/* global shallow, describe, it, expect */
import React from 'react'
import { AddTodoItem } from './addTodoItem'
import { EditModal } from './editModal'
import { SearchBar } from './searchBar'
import { ShowStatuses } from './showStatuses'
import { TodoItem } from './todoItem'

describe('Components', () => {
  it('AddTodoItem', () => {
    const rendered = shallow(<AddTodoItem />)
    expect(rendered).toMatchSnapshot()
  })
  it('EditModal', () => {
    const rendered = shallow(<EditModal />)
    expect(rendered).toMatchSnapshot()
  })
  it('SearchBar', () => {
    const rendered = shallow(<SearchBar />)
    expect(rendered).toMatchSnapshot()
  })
  it('ShowStatuses', () => {
    const rendered = shallow(<ShowStatuses />)
    expect(rendered).toMatchSnapshot()
  })
  it('TodoItem', () => {
    const rendered = shallow(<TodoItem item={{ id: 1, name: 'aaa', completed: false }} />)
    expect(rendered).toMatchSnapshot()
  })
})
