import React, { useState } from 'react'
import { Modal, withStyles, Paper, TextField, Button } from '@material-ui/core'
import PropTypes from 'prop-types'

const styles = ({ spacing }) => ({
  content: {
    top: '50%',
    left: '50%',
    transform: `translate(-50%, -50%)`,
    position: 'absolute',
    width: 400,
    height: 'auto',
    padding: spacing(3),
    outline: 'none',
    display: 'flex',
    justifyContent: 'space-around',
    alighItems: 'center'
  }
})

const EditModalRender = ({ classes, editTodoItem, modalOpen, closeModal }) => {
  const [textInput, setTextInput] = useState('')

  const handleOnClick = () => {
    editTodoItem(textInput)
    setTextInput('')
  }

  return <Modal
    open={modalOpen}
    onClose={() => closeModal()}
  >
    <Paper className={classes.content}>
      <TextField label={'Change TODO Item'} value={textInput} onChange={event => setTextInput(event.target.value)} />
      <Button variant={'contained'} color={'primary'} onClick={() => handleOnClick()}>
        Edit
      </Button>
    </Paper>
  </Modal>
}

EditModalRender.propTypes = {
  classes: PropTypes.object,
  editTodoItem: PropTypes.func,
  modalOpen: PropTypes.bool,
  closeModal: PropTypes.func
}
export const EditModal = withStyles(styles)(EditModalRender)
