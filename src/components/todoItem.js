import React from 'react'
import PropTypes from 'prop-types'
import { IconButton, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import { Delete as DeleteIcon, Done as DoneIcon, Edit as EditIcon } from '@material-ui/icons'

export const TodoItem = ({ item, openModal, setTodoItemCompleted, removeTodoItem }) => (
  <ListItem disabled={item.completed}>
    <ListItemText primary={item.name} />
    <ListItemSecondaryAction>
      <IconButton disabled={item.completed} onClick={() => openModal(item.id)}>
        <EditIcon />
      </IconButton>
      <IconButton onClick={() => setTodoItemCompleted(item.id)} disabled={item.completed}>
        <DoneIcon />
      </IconButton>
      <IconButton edge={'end'} onClick={() => removeTodoItem(item.id)}>
        <DeleteIcon />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
)

TodoItem.propTypes = {
  item: PropTypes.object,
  openModal: PropTypes.func,
  setTodoItemCompleted: PropTypes.func,
  removeTodoItem: PropTypes.func
}
