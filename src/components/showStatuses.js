import React from 'react'
import PropTypes from 'prop-types'
import { Checkbox, FormControlLabel, Grid } from '@material-ui/core'

export const ShowStatuses = ({ showCompletedTasks, setShowCompletedTasks, showInCompletedTasks, setShowInCompletedTasks }) => (
  <Grid item sm={6}>
    <FormControlLabel control={
      <Checkbox
        onChange={() => setShowCompletedTasks(!showCompletedTasks)}
        value={showCompletedTasks}
      />
    } label={'Completed Tasks'} />
    <FormControlLabel control={
      <Checkbox
        onChange={() => setShowInCompletedTasks(!showInCompletedTasks)}
        value={showInCompletedTasks}
      />
    } label={'Incompleted Tasks'} />
  </Grid>
)

ShowStatuses.propTypes = {
  showCompletedTasks: PropTypes.bool,
  setShowCompletedTasks: PropTypes.func,
  showInCompletedTasks: PropTypes.bool,
  setShowInCompletedTasks: PropTypes.func
}
