import React from 'react'
import PropTypes from 'prop-types'
import { Button, Grid, TextField } from '@material-ui/core'

export const AddTodoItem = ({ todoInput, setTodoInput, addTodoItem }) => (
  <Grid container item sm={4} alignItems={'flex-end'}>
    <Grid item sm={8}>
      <TextField label={'Add TODO item'} value={todoInput} onChange={event => setTodoInput(event.target.value)} />
    </Grid>
    <Grid item sm={4}>
      <Button variant={'contained'} color={'primary'} disabled={!todoInput} onClick={() => addTodoItem(todoInput)}>
        Add
      </Button>
    </Grid>
  </Grid>
)

AddTodoItem.propTypes = {
  todoInput: PropTypes.string,
  setTodoInput: PropTypes.func,
  addTodoItem: PropTypes.func
}
