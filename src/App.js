import React from 'react'
import { ThemeProvider } from '@material-ui/styles'
import { HomePage } from './containers/homepage'
import { theme } from './components/theme'

function App () {
  return (
    <ThemeProvider theme={theme}>
      <HomePage />
    </ThemeProvider>
  )
}

export default App
