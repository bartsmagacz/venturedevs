import React, { useState, useEffect } from 'react'
import { Container,
  List,
  Grid
} from '@material-ui/core'
import { getRandomId, useStateFromLocalStorage } from '../utils'
import { EditModal } from '../components/editModal'
import { TodoItem } from '../components/todoItem'
import { AddTodoItem } from '../components/addTodoItem'
import { ShowStatuses } from '../components/showStatuses'
import { SearchBar } from '../components/searchBar'

const HomePageRender = () => {
  const [todoItems, setTodoItems] = useStateFromLocalStorage('todoItems', [])
  const [todoInput, setTodoInput] = useState('')
  const [modalOpen, setModalOpen] = useState(false)
  const [todoItemToEdit, setTodoItemToEdit] = useState()
  const [searchItem, setSearchItem] = useState('')
  const [filteredTodoItems, setFilteredTodoItems] = useState(todoItems)
  const [showCompletedTasks, setShowCompletedTasks] = useState(false)
  const [showInCompletedTasks, setShowInCompletedTasks] = useState(false)

  const addTodoItem = (todoItem) => {
    setTodoItems([...todoItems, { id: getRandomId(), name: todoItem, completed: false }])
    setTodoInput('')
  }

  const removeTodoItem = (itemToRemove) => {
    setTodoItems(todoItems.filter((item) => item.id !== itemToRemove))
  }

  const setTodoItemCompleted = (itemToCompletedId) => {
    setTodoItems(todoItems.map((item) => {
      if (item.id === itemToCompletedId) item.completed = true
      return item
    }))
  }

  const editTodoItem = (itemId) => {
    return (itemText) => {
      setTodoItems(todoItems.map((item) => {
        if (item.id === itemId) {
          item.name = itemText
          setModalOpen(false)
        }
        return item
      }))
    }
  }

  const openModal = (itemId) => {
    setTodoItemToEdit(itemId)
    setModalOpen(true)
  }

  const closeModal = () => {
    setTodoItemToEdit(null)
    setModalOpen(false)
  }

  useEffect(() => {
    const filteredItems = (searchItem) => {
      const searchedItems = todoItems.filter((item) => item.name.toLowerCase().search(searchItem.toLowerCase()) !== -1)
      setFilteredTodoItems(searchedItems.filter((item) => {
        if (showCompletedTasks || showInCompletedTasks) {
          if (item.completed === showCompletedTasks) return true
          if (item.completed !== showInCompletedTasks) return true
        } else {
          return true
        }
      }))
    }

    filteredItems(searchItem)
  }, [todoItems, searchItem, showCompletedTasks, showInCompletedTasks])

  return <Container>
    <Grid container alignItems={'flex-end'}>
      <AddTodoItem todoInput={todoInput} setTodoInput={setTodoInput} addTodoItem={addTodoItem} />
      <ShowStatuses showCompletedTasks={showCompletedTasks}
        setShowCompletedTasks={setShowCompletedTasks}
        showInCompletedTasks={showInCompletedTasks}
        setShowInCompletedTasks={setShowInCompletedTasks} />
      <SearchBar searchItem={searchItem} setSearchItem={setSearchItem} />
    </Grid>
    <List>
      { filteredTodoItems.map((item) => (
        <TodoItem item={item} key={item.id} openModal={openModal} setTodoItemCompleted={setTodoItemCompleted} removeTodoItem={removeTodoItem} />
      )) }
    </List>
    <EditModal modalOpen={modalOpen} closeModal={closeModal} todoItemToEdit={todoItemToEdit} editTodoItem={editTodoItem(todoItemToEdit)} />
  </Container>
}

export const HomePage = HomePageRender
