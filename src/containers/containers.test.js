// eslint-disable-next-line no-unused-vars
/* global shallow, describe, it, expect */
import React from 'react'
import { HomePage } from './homepage'

describe('Containers', () => {
  it('HomePage', () => {
    const rendered = shallow(<HomePage />)
    expect(rendered).toMatchSnapshot()
  })
})
