## Venture Devs TODO App

#### To Run:

``docker-compose up app``
or
``npm install && npm run start``

#### To run tests:

``docker-compose run tests``
or
``npm install && npm run test``

#### Main Technologies

- ReactJS
- Material UI
- ESLint (StandardJS)
- Docker (Docker-compose)
- Jest

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
